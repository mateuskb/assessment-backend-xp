<?php

    include_once __dir__.'/../consts/consts.php';

    /**
     * Deleta produto
     * Request
     * 
     * @param int $id
    */
    include_once("../classes/Produtos.php");

    $id = isset($_GET['id']) ? $_GET["id"] : 0;


    try{
        $id = intval($id);

        $resp = (new Produtos)->delete_image($id);

        if($resp["ok"]):
            header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&successImage=1");
        else:
            header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&successImage=-1");
        endif;
        
    } catch(Exception $e){
        header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&successImage=-1");
    }

    