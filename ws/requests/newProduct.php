<?php

    include_once __dir__.'/../consts/consts.php';

    /**
     * Criar novo produto
     * Request
     * 
     * @param string $nome
     * @param string $descricao 
     * @param string $sku
     * @param float $price
     * @param int $quantidade
     * @param file $imagem
     * @param array $categorias - Array de ids
    */
    include_once("../classes/Produtos.php");

    $nome = isset($_POST["nome"]) ? $_POST["nome"] : "";
    $sku = isset($_POST['sku']) ? $_POST["sku"] : "";
    $price = isset($_POST['price']) ? $_POST["price"] : 0;
    $quantidade = isset($_POST['quantidade']) ? $_POST["quantidade"] : 0;
    $categorias = isset($_POST['categorias']) ? $_POST["categorias"] : [];
    $descricao = isset($_POST['descricao']) ? $_POST["descricao"] : [];
    $file = isset($_FILES['imagem']) ? $_FILES["imagem"] : "";
    $imagem = isset($file["name"]) ? $file["name"] : "";

    try{
        
        $resp = (new Produtos)->criar($nome, $sku, $price, $quantidade, $categorias, $descricao, $imagem);

        if($resp["ok"]):
            //codigo de upar as fotos
            $uploadfile = UPLOAD_LINK . basename($imagem);
            if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
                header("Location: ".PAGES["AddProduct"]."?success=1");
            } else {
                header("Location: ".PAGES["AddProduct"]."?success=-1");
            }
        else:
            header("Location: ".PAGES["AddProduct"]."?success=-1");
        endif;

    } catch(Exception $e){
        header("Location: ".PAGES["AddProduct"]."?success=-1");
    }

    