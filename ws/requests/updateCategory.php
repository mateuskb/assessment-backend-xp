<?php

    include_once __dir__.'/../consts/consts.php';
    include_once("../classes/Categorias.php");

    /**
     * Update category
     * Request
     * 
     * @param int $id
     * @param string $nome
     * @param string $code
    */

    $id = isset($_GET["id"]) ? $_GET["id"] : 0;
    $nome = isset($_POST["nome"]) ? $_POST["nome"] : "";
    $code = isset($_POST['code']) ? $_POST["code"] : [];

    try{
        $id = intval($id);
        
        $resp = (new Categorias)->update($id, $nome, $code);

        if($resp["ok"]):
            header("Location: ".PAGES["UpdateCategory"]."?id=".$id."&success=1");
        else:
            header("Location: ".PAGES["UpdateCategory"]."?id=".$id."&success=-1");
        endif;

    } catch(Exception $e){
        header("Location: ".PAGES["UpdateCategory"]."?id=".$id."&success=-1");
    }

    