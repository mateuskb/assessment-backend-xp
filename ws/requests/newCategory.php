<?php

    include_once __dir__.'/../consts/consts.php';

    /**
     * Criar nova categoria
     * Request
     * 
     * @param string $nome
     * @param string $code
    */
    include_once("../classes/Categorias.php");

    $nome = isset($_POST["nome"]) ? $_POST["nome"] : "";
    $code = isset($_POST['code']) ? $_POST["code"] : "";

    try{
        
        $resp = (new Categorias)->criar($nome, $code);

        // var_dump(($resp));
        if($resp["ok"]):
            header("Location: ".PAGES["AddCategory"]."?success=1");
        else:
            header("Location: ".PAGES["AddCategory"]."?success=-1");
        endif;

    } catch(Exception $e){
        header("Location: ".PAGES["AddCategory"]."?success=-1");
    }

    