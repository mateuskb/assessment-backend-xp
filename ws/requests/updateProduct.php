<?php

    include_once __dir__.'/../consts/consts.php';

    /**
     * Update novo produto
     * Request
     * 
     * @param int $id
     * @param string $nome
     * @param string $descricao - Array de ids
     * @param string $sku
     * @param float $price
     * @param int $quantidade
     * @param array $categorias - Array de ids
    */
    include_once("../classes/Produtos.php");

    $id = isset($_GET["id"]) ? $_GET["id"] : 0;
    $nome = isset($_POST["nome"]) ? $_POST["nome"] : "";
    $sku = isset($_POST['sku']) ? $_POST["sku"] : "";
    $price = isset($_POST['price']) ? $_POST["price"] : 0;
    $quantidade = isset($_POST['quantidade']) ? $_POST["quantidade"] : 0;
    $categorias = isset($_POST['categorias']) ? $_POST["categorias"] : [];
    $descricao = isset($_POST['descricao']) ? $_POST["descricao"] : [];
    $file = isset($_FILES['imagem']) ? $_FILES["imagem"] : "";
    $imagem = isset($file["name"]) ? $file["name"] : "";


    try{
        $id = intval($id);
        

        if(empty($imagem)):
            $resp = (new Produtos)->update($id, $nome, $sku, $price, $quantidade, $categorias, $descricao, $imagem);
            if($resp["ok"]):
                header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&success=1");
            else:
                header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&success=-1");
            endif;
        else:
            $resp = (new Produtos)->delete_image($id);
            if($resp["ok"]):
                $resp = (new Produtos)->update($id, $nome, $sku, $price, $quantidade, $categorias, $descricao, $imagem);
                if($resp["ok"]):
                    $uploadfile = UPLOAD_LINK . basename($imagem);
                    if (move_uploaded_file($file['tmp_name'], $uploadfile)):
                        header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&success=1");
                    else:
                        header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&success=-1");
                    endif;
                else:
                    header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&success=-1");
                endif;
            else:
                header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&success=-1");
            endif;
        endif;
    } catch(Exception $e){
        header("Location: ".PAGES["UpdateProduct"]."?id=".$id."&success=-1");
    }

    