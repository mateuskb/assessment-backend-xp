<?php

    include_once __dir__.'/../consts/consts.php';

    /**
     * Deleta categoria
     * Request
     * 
     * @param int $id
    */
    include_once("../classes/Categorias.php");

    $id = isset($_GET['id']) ? $_GET["id"] : 0;


    try{
        $id = intval($id);

        $resp = (new Categorias)->delete($id);

        
        if($resp["ok"]):
            header("Location: ".PAGES["Categorias"]."?success=1");
        else:
            header("Location: ".PAGES["Categorias"]."?success=-1");
        endif;

    } catch(Exception $e){
        header("Location: ".PAGES["Categorias"]."?success=-1");
    }

    