<?php

    include_once __dir__.'/../consts/consts.php';

    /**
     * Teste automatizado 
     * Request
     * 
     * O teste irá iterar através do arquivo import.csv e inserir os dados no banco de dados.
    */
    include_once("../classes/Produtos.php");
    include_once("../classes/Categorias.php");

    
    try{
        $produtos_criados = 0;
        $categorias_criadas = 0;
        $row = 0;
        $produtos = [];
        $categorias = [];
        $id_categorias = [];
        
        if (($handle = fopen("../../assets/import.csv", "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
                $id_categorias_per_row = [];

                if($row > 0):
                    if($row > 0):
                        // PULA PRIMEIRA LINHA
                        
                        // CRIAR CATEGORIA SE AINDA NAO EXISTIR
                        if($data[5] != "(no genres listed)" && $data[5] != ""):
                            // TEM CATEGORIA
                            $categories_arr = explode('|', $data[5]);
    
                            foreach($categories_arr as $category):

                                if(!in_array($category, $categorias)):

                                    // CRIAR CATEGORIA SE AINDA NAO FOR CRIADA
                                    // echo $category."<br/>";
                                    $resp = (new Categorias)->criar($category, "Criada automaticamente");

                                    // var_dump(($resp));
                                    if($resp["ok"]):
                                        $categorias_criadas++;
                                        $categorias[] = $category;
                                        $id_categorias[] = $resp["data"];

                                    endif;

                                endif;

                                // echo $category." - ";
                                // echo array_search($category, $categorias)." - ";
                                // echo $id_categorias[array_search($category, $categorias)]."<br/>";
                                $id_categorias_per_row[] = $id_categorias[array_search($category, $categorias)];
                                
                            endforeach;
                            // MOSTAR CATEGORIAS DE CADA LINHA
                            // echo "<pre>";
                            // var_dump($id_categorias_per_row);
                            // echo "<pre/>";
                            // $resp = (new Produtos())->criar($category, "Criada automaticamente");
                            $resp = (new Produtos)->criar($data[0], $data[1], $data[4], $data[3], $id_categorias_per_row, $data[2], "");

                            if($resp["ok"]):
                                $produtos[] = $data[0];
                                $produtos_criados++;
                            endif;
    
                        endif;
    
                    endif;
                    // echo '<br>';
                endif;
                $row++;

                // $num = count($data);
                // echo "<p> $num campos na linha $row: <br /></p>\n";
                // $row++;
                // for ($c=0; $c < $num; $c++) {
                //     echo $data[$c] . "<br />\n";
                // }
            }
            fclose($handle);
            echo "<br/>";
            echo "Quantidade de produtos criados: ".$produtos_criados;
            echo "<br/>";
            echo "Quantidade de categorias criadas: ".$categorias_criadas;
            echo "<br/>";
            echo "Categorias: ";
            echo "<pre>";
            var_dump($categorias);
            echo "<pre/>";
            echo "<br/>";
            echo "Produtos: ";
            echo "<pre>";
            var_dump($produtos);
            echo "<pre/>";
        }
    } catch(Exception $e){
        echo '<script type="text/javascript">alert("Erro ao rodar teste: '. $e.'");</script>';
    }

    