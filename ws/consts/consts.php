<?php
    define('IMAGES_TYPES', ['png', 'jpeg', 'jpg', 'gif', 'PNG', 'JPEG', 'JPG', 'GIF']);
    define('MAX_IMAGE_PER_PROJECT', 3);
    
    define('SERVERHOST', 'localhost');
    define('USERNAME', 'root'); 
    define('PASSWORD', '');
    define('DB', 'assessment');

    define("WEB_LINK", "http://localhost/assessment-backend-xp");
    define("UPLOAD_LINK", $_SERVER['DOCUMENT_ROOT']."/assessment-backend-xp/ws/uploads/");
    define("UPLOAD_IMAGES", "http://localhost//assessment-backend-xp/ws/uploads/");

    define("PAGES", [
        "Categorias" => WEB_LINK."/assets/categories.php",
        "Produtos" => WEB_LINK."/assets/products.php",
        "AddProduct" => WEB_LINK."/assets/addProduct.php",
        "AddCategory" => WEB_LINK."/assets/addCategory.php",
        "UpdateProduct" => WEB_LINK."/assets/updateProduct.php",
        "UpdateCategory" => WEB_LINK."/assets/updateCategory.php",
        "Dashboard" => WEB_LINK."/assets/dashboard.php"
    ]);
        
    define("REQUESTS", [
        "delImage" => WEB_LINK."/ws/requests/delImage.php",
        "updateProduct" => WEB_LINK."/ws/requests/updateProduct.php",
        "updateCategory" => WEB_LINK."/ws/requests/updateCategory.php",
        "newProduct" => WEB_LINK."/ws/requests/newProduct.php",
        "newCategory" => WEB_LINK."/ws/requests/newCategory.php",
        "delProduct" => WEB_LINK."/ws/requests/delProduct.php",
        "delCategoria" => WEB_LINK."/ws/requests/delCategoria.php"
    ]);