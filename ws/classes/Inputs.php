<?php 

/**
 * Inputs
 * Verifica dados vindos do cliente
 * 
*/
class Inputs {

    /**
     * 
     * Verifica valores strings
     *
     * @param array $inputs
     * 
     * $inputs
     *       0 -> Nome para mostra de erro
     *       1 -> Variavel
     * 
     * @return array $errors Array de erros, caso existir
    */
    public function verify_strings($inputs){
        $errors = [];

        foreach($inputs as $input):
            try{
                if(is_string($input[1])):
                    if(empty($input[1])):
                        $errors[$input[0]] = $input[0].' não indicado.';
                    else:
                        if(isset($input[2])):
                            if ( preg_match($input[2],$input[1])):
                                $errors[$input[0]] = $input[0].' inválida.';
                            endif;
                        endif;
                    endif;
                else:
                    $errors[$input[0]] = $input[0].' não é string.';
                endif;
            } catch(Exception $e) {
                $errors['exception'] = $e;
            }
        endforeach;

        return $errors;
    }

    /**
     * 
     * Verifica valores inteiros ou floats
     *
     * @param array $inputs
     * 
     * $inputs
     *       0 -> Nome
     *       1 -> Variavel
     *       2 -> Simbolo
     *           '+' -> Apenas positivos
     *           '-' -> Apenas negativos
     *           '*' -> Diferentes de 0
     *       3 -> Valor maximo
     * 
     * @return array $errors Array de erros, caso existir
    */
    public function verify_numbers($inputs){
        $errors = [];
        
        foreach($inputs as $input):
            try{
                if(isset($input[2])):
                    if(strpos($input[2], '*') !== false):
                        if($input[1] == 0):
                            $errors[$input[0]] = $input[0].' inválido.';
                        endif;
                    endif;
                    if((strpos($input[2], '+') !== false) && (!strpos($input[2], '-') !== false)):
                        if($input[1] < 0):
                            $errors[$input[0]] = $input[0].' inválido.';
                        endif; 
                    endif;
                    if((strpos($input[2], '-') !== false) && (!strpos($input[2], '+') !== false)):
                        if($input[1] > 0):
                            $errors[$input[0]] = $input[0].' inválido.';
                        endif; 
                    endif;
                endif;
                if(isset($input[3])):
                    if($input[1] > $input[3]):
                        $errors[$input[0]] = $input[0].' inválido.';
                    endif;
                endif;
            } catch(Exception $e) {
                $errors['exception'] = $e;
            }
        endforeach;

        return $errors;
    }
}
