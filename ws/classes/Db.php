<?php

include_once __dir__.'/../consts/consts.php';

/**
 * Db
 * Conecta ao banco de dados
 * 
*/
class Db 
{

    function __construct() {
        
        $this->username = USERNAME;
        $this->password = PASSWORD;
        $host = SERVERHOST;
        $db = DB;

        $this->conn_str = "mysql:host=$host;dbname=$db;charset=utf8mb4";
    }

    /**
     * 
     * Conecta ao banco de dados
     *
     * @return object com conexao ou false
     */
    public function connect(){
        try {
            $conn = new PDO($this->conn_str, $this->username, $this->password);
            // Muda o modo de erro do PDO para excecption
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        }catch(PDOException $e) {
            file_put_contents(__dir__."/../logs/errors.log", date("j.n.Y")." - ".$e, FILE_APPEND);
            return false;
        }
    }
}