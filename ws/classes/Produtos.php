<?php
require_once(__dir__.'/Inputs.php');
require_once(__dir__.'/Db.php');

/**
 * Produtos
 * CRUD de produtos
 * @param object $_conn nova conexao com o banco de dados (opcional)
 * 
 * 
*/
class Produtos 
{

    /**
     * __construct
     * Conexão com o banco de dados
     * Cria uma nova conexão caso não for passada anteriormente
    */
    var $conn;
    function __construct($_conn=NULL) {
        if($_conn):
            try{
                $this->conn = is_object($_conn) ? $_conn : (new Db())->connect();
            }catch (Exception $e){
                $this->conn = false;
            }
        else:
            try{
                $this->conn = (new Db())->connect();
            }catch (Exception $e){
                $this->conn = false;
            }
        endif;
    }


    /**
     * 
     * Cria um novo produto
     *
     * @param string $nome
     * @param string $sku
     * @param string $descricao
     * @param float $price
     * @param int $quantidade
     * @param array $categorias - Array de ids
     * 
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> int -> Retorna o id do produto criado
     * 
    */
    function criar($nome, $sku, $price, $quantidade, $categorias, $descricao, $imagem) {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>0
        ];

        // validar inputs
        $resp = (new Inputs)->verify_strings([
            ['Nome', $nome],
            ['Sku', $sku]
        ]);

        $data['errors'] = array_merge($data['errors'], $resp);
        
        $resp = (new Inputs)->verify_numbers([
            ['Preço', $price, '+'],
            ['Quantidade', $quantidade, '+*']
        ]);
            
        $data['errors'] = array_merge($data['errors'], $resp);

        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                $dt_now = (new DateTime('now', new DateTimeZone("UTC")));
                $dt_now = $dt_now->format("Y-m-d H:i:s"); 
                
                if(empty($data['errors'])):
                    
                    $sql = '
                        INSERT 
                            produtos (
                                pro_c_sku,
                                pro_c_nome,
                                pro_i_quantidade,
                                pro_d_price,
                                pro_dt_criado_em,
                                pro_t_descricao,
                                pro_c_imagem
                        ) VALUES (
                                :sku,
                                :nome,
                                :quantidade,
                                :price,
                                :dt_now,
                                :descricao,
                                :imagem
                            );
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':sku', $sku, PDO::PARAM_STR);                        
                    $stmt->bindValue(':nome', $nome, PDO::PARAM_STR);                        
                    $stmt->bindValue(':quantidade', $quantidade, PDO::PARAM_INT);                        
                    $stmt->bindValue(':price', $price, PDO::PARAM_STR);                        
                    $stmt->bindValue(':dt_now', $dt_now, PDO::PARAM_STR);                        
                    $stmt->bindValue(':descricao', $descricao, PDO::PARAM_STR);                        
                    $stmt->bindValue(':imagem', $imagem, PDO::PARAM_STR);                        
                    $stmt->execute();
                    $id_produto = $this->conn->lastInsertId();

                    if($id_produto > 0):

                        if(is_array($categorias)):

                            foreach($categorias as $id_categoria):
                                if($id_categoria > 0):
                                    $sql = '
                                        INSERT 
                                            pro_cat (
                                                pc_fk_produto,
                                                pc_fk_categoria
                                        ) VALUES (
                                                :id_produto,
                                                :id_categoria
                                            );
                                    ';
                                    $stmt = $this->conn->prepare($sql);
                                    $stmt->bindValue(':id_produto', $id_produto, PDO::PARAM_INT);                        
                                    $stmt->bindValue(':id_categoria', $id_categoria, PDO::PARAM_INT);                        
                                    $stmt->execute();
                                endif;
                            endforeach;
                            $data['data'] = $id_produto;
                        else:
                            $data["errors"]["categorias"] = "Categorias não indicadas";
                        endif;
                    endif;
                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }
    
    /**
     * 
     * Ler produtos
     *
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> array -> Retorna produtos
     * 
    */
    function ler() {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>[]
        ];

        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                if(empty($data['errors'])):
                    
                    $sql = '
                        SELECT
                            *
                        FROM
                            produtos
                        ORDER BY
                            pro_pk DESC
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                    $produtos = $stmt->fetchAll();

                    if(isset($produtos)):
                        foreach($produtos as $produto):
                            $sql = '
                                SELECT
                                    *
                                FROM
                                    categorias
                                LEFT JOIN
                                    pro_cat ON pc_fk_categoria = cat_pk
                                WHERE
                                    pc_fk_produto = :id_produto
                            ';
                            $stmt = $this->conn->prepare($sql);
                            $stmt->bindValue(':id_produto', $produto["pro_pk"], PDO::PARAM_INT);                        
                            $stmt->execute();
                            $categorias = $stmt->fetchAll();

                            $produto["categorias"] = $categorias;
                            $data["data"][] = $produto;
                        endforeach;
                    endif;
                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }


    /**
     * 
     * Ler apenas um produto
     *
     * @param int $id Id do produto a ser lido
     * 
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> array -> Retorna o  produto indicado
     * 
    */
    function ler_produto($id) {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>[]
        ];

        $resp = (new Inputs)->verify_numbers([
            ['Idproduto', $id, '+*']
        ]);
            
        $data['errors'] = array_merge($data['errors'], $resp);

        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                if(empty($data['errors'])):
                    
                    $sql = '
                        SELECT
                            *
                        FROM
                            produtos
                        WHERE
                            pro_pk = :id_produto
                        ORDER BY
                            pro_pk DESC
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_produto', $id, PDO::PARAM_INT);                        
                    $stmt->execute();
                    $produto =  $stmt->fetch();

                    $sql = '
                        SELECT
                            cat_pk
                        FROM
                            categorias
                        LEFT JOIN
                            pro_cat ON pc_fk_categoria = cat_pk
                        WHERE
                            pc_fk_produto = :id_produto
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_produto', $id, PDO::PARAM_INT);                        
                    $stmt->execute();
                    $categorias = $stmt->fetchAll();

                    $produto["categorias"] = $categorias;
                    $data['data'] = $produto;
                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }


    /**
     * 
     * Atualiza um produto
     *
     * @param int $id
     * @param string $nome
     * @param string $sku
     * @param string $descricao
     * @param float $price
     * @param int $quantidade
     * @param array $categorias - Array de ids
     * 
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> boolean -> Retorna se o update funcionou
     * 
    */
    function update($id, $nome, $sku, $price, $quantidade, $categorias, $descricao, $imagem) {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>false
        ];

        
        // validar inputs
        $resp = (new Inputs)->verify_strings([
            ['Nome', $nome],
            ['Sku', $sku]
        ]);
        
        $data['errors'] = array_merge($data['errors'], $resp);
        
        $resp = (new Inputs)->verify_numbers([
            ['Id', $id, '+*'],
            ['Preço', $price, '+'],
            ['Quantidade', $quantidade, '+*']
        ]);
                
        $data['errors'] = array_merge($data['errors'], $resp);
        
        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):

            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                if(empty($data['errors'])):
                    
                    $sql = '
                        UPDATE 
                            produtos 
                        SET
                            pro_c_sku = :sku,
                            pro_c_nome = :nome,
                            pro_i_quantidade = :quantidade,
                            pro_d_price = :price,
                            pro_t_descricao = :descricao';
                    if(!empty($imagem)):
                        $sql .= '
                            ,pro_c_imagem = :imagem
                        ';
                    endif;
                    $sql .= '
                        WHERE
                            pro_pk = :id_produto
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_produto', $id, PDO::PARAM_INT);                        
                    $stmt->bindValue(':sku', $sku, PDO::PARAM_STR);                        
                    $stmt->bindValue(':nome', $nome, PDO::PARAM_STR);                        
                    $stmt->bindValue(':quantidade', $quantidade, PDO::PARAM_INT);                        
                    $stmt->bindValue(':price', $price, PDO::PARAM_STR);                        
                    $stmt->bindValue(':descricao', $descricao, PDO::PARAM_STR);      
                    if(!empty($imagem)):
                        $stmt->bindValue(':imagem', $imagem, PDO::PARAM_STR);                        
                    endif;                  
                    $stmt->execute();

                    if(is_array($categorias)):
                        
                        $sql = '
                            DELETE FROM 
                                pro_cat
                            WHERE
                                pc_fk_produto = :id_produto
                        ';
                        $stmt = $this->conn->prepare($sql);
                        $stmt->bindValue(':id_produto', $id, PDO::PARAM_INT);                        
                        $stmt->execute();

                        foreach($categorias as $id_categoria):
                            
                            if($id_categoria > 0):

                                $sql = '
                                    INSERT 
                                        pro_cat (
                                            pc_fk_produto,
                                            pc_fk_categoria
                                    ) VALUES (
                                            :id_produto,
                                            :id_categoria
                                        );
                                ';
                                $stmt = $this->conn->prepare($sql);
                                $stmt->bindValue(':id_produto', $id, PDO::PARAM_INT);                        
                                $stmt->bindValue(':id_categoria', $id_categoria, PDO::PARAM_INT);                        
                                $stmt->execute();

                            endif;
                        endforeach;
                    else:
                        $data["errors"]["categorias"] = "Categorias não indicadas";
                    endif;
                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            $data['data'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }
    
    /**
     * 
     * Deleta uma imagem de produto
     *
     * @param int $id id do produto
     * 
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> boolean -> Retorna se o delete funcionou
     * 
    */
    function delete_image($id) {
        
        $imagem = "";
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>false
        ];

        // validar inputs
        $resp = (new Inputs)->verify_numbers([
            ['Id', $id, '+*']
        ]);
            
        $data['errors'] = array_merge($data['errors'], $resp);

        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                if(empty($data['errors'])):
                    $sql = '
                        SELECT
                            pro_c_imagem
                        FROM
                            produtos
                        WHERE
                            pro_pk = :id_produto
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_produto', $id, PDO::PARAM_INT);                        
                    $stmt->execute();
                    $filename = $stmt->fetchColumn();
                    
                    if(!empty($filename)):

                        $sql = '
                            UPDATE
                                produtos
                            SET
                                pro_c_imagem = ""
                            WHERE
                                pro_pk = :id_produto
                        ';
                        $stmt = $this->conn->prepare($sql);
                        $stmt->bindValue(':id_produto', $id, PDO::PARAM_INT);                        
                        $stmt->execute();

                        
                    endif;
                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
            
            if(empty($data['errors'])):
                if(!empty($filename)):
                    if (file_exists(UPLOAD_LINK.$filename)):
                        unlink(UPLOAD_LINK.$filename);
                    else :
                        $data['errors']['db'] = 'Erro ao deletar imagem!';
                    endif;
                endif;
            endif;
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            $data['data'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }

    /**
     * 
     * Deleta um produto
     *
     * @param int $id
     * 
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> boolean -> Retorna se o delete funcionou
     * 
    */
    function delete($id) {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>false
        ];

        // validar inputs
        $resp = (new Inputs)->verify_numbers([
            ['Id', $id, '+*']
        ]);
            
        $data['errors'] = array_merge($data['errors'], $resp);

        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                if(empty($data['errors'])):
                    $sql = '
                        DELETE FROM 
                            pro_cat
                        WHERE
                            pc_fk_produto = :id_produto
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_produto', $id, PDO::PARAM_INT);                        
                    $stmt->execute();
                    
                    $sql = '
                        DELETE FROM
                            produtos
                        WHERE
                            pro_pk = :id_produto
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_produto', $id, PDO::PARAM_INT);                        
                    $stmt->execute();

                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            $data['data'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }
}
