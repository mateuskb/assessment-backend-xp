<?php
require_once(__dir__.'/Db.php');
require_once(__dir__.'/Inputs.php');

/**
 * Categorias
 * CRUD de Categorias
 * @param object $_conn nova conexao com o banco de dados (opcional)
 * 
 * 
*/
class Categorias 
{

    /**
     * __construct
     * Conexão com o banco de dados
     * Cria uma nova conexão caso não for passada anteriormente
    */
    var $conn;
    function __construct($_conn=NULL) {
        if($_conn):
            try{
                $this->conn = is_object($_conn) ? $_conn : (new Db())->connect();
            }catch (Exception $e){
                $this->conn = false;
            }
        else:
            try{
                $this->conn = (new Db())->connect();
            }catch (Exception $e){
                $this->conn = false;
            }
        endif;
    }


    /**
     * 
     * Cria um novo categoria
     *
     * @param string $nome
     * @param string $code
     * 
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> int -> Retorna o id do categoria criado
     * 
    */
    function criar($nome, $code) {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>0
        ];

        // validar inputs
        $resp = (new Inputs)->verify_strings([
            ['Nome', $nome],
            ['Code', $code],
            ]);

        $data['errors'] = array_merge($data['errors'], $resp);
        
        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                $dt_now = (new DateTime('now', new DateTimeZone("UTC")));
                $dt_now = $dt_now->format("Y-m-d H:i:s"); 
                
                if(empty($data['errors'])):
                    
                    $sql = '
                        INSERT 
                            categorias (
                                cat_c_nome,
                                cat_c_code
                        ) VALUES (
                                :nome,
                                :code
                            );
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':nome', $nome, PDO::PARAM_STR);                        
                    $stmt->bindValue(':code', $code, PDO::PARAM_STR);                        
                    $stmt->execute();

                    $data["data"] = $this->conn->lastInsertId();
                endif;

            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }
    
    /**
     * 
     * Ler categorias
     *
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> array -> Retorna categorias
     * 
    */
    function ler() {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>[]
        ];

        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                if(empty($data['errors'])):
                    
                    $sql = '
                        SELECT
                            *
                        FROM
                            categorias
                        ORDER BY
                            cat_pk DESC
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                    $data['data'] =  $stmt->fetchAll();

                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }

    /**
     * 
     * Ler apenas uma categoria
     *
     * @param int $id Id da categoria a ser lida
     * 
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> array -> Retorna a categoria indicada
     * 
    */
    function ler_categoria($id) {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>[]
        ];

        $resp = (new Inputs)->verify_numbers([
            ['Idproduto', $id, '+*']
        ]);
            
        $data['errors'] = array_merge($data['errors'], $resp);

        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                if(empty($data['errors'])):
                    
                    $sql = '
                        SELECT
                            *
                        FROM
                            categorias
                        WHERE
                            cat_pk = :ic_categoria
                        ORDER BY
                            cat_pk DESC
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':ic_categoria', $id, PDO::PARAM_INT);                        
                    $stmt->execute();
                    $categoria =  $stmt->fetch();
                    $data['data'] = $categoria;
                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }


    /**
     * 
     * Atualiza um categoria
     *
     * @param int $id
     * @param string $nome
     * @param string $code
     * 
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> boolean -> Retorna se o update funcionou
     * 
    */
    function update($id, $nome, $code) {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>false
        ];

        // validar inputs
        $resp = (new Inputs)->verify_strings([
            ['Nome', $nome],
            ['Code', $code]
        ]);

        $data['errors'] = array_merge($data['errors'], $resp);
        
        $resp = (new Inputs)->verify_numbers([
            ['Id', $id, '+*']
        ]);
            
        $data['errors'] = array_merge($data['errors'], $resp);

        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                if(empty($data['errors'])):
                    
                    $sql = '
                        UPDATE 
                            categorias 
                        SET
                            cat_c_code = :code,
                            cat_c_nome = :nome
                        WHERE
                            cat_pk = :id_categoria
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_categoria', $id, PDO::PARAM_INT);                        
                    $stmt->bindValue(':code', $code, PDO::PARAM_STR);                        
                    $stmt->bindValue(':nome', $nome, PDO::PARAM_STR);                        
                    $stmt->execute();

                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            $data['data'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }
    
    /**
     * 
     * Deleta um produto
     *
     * @param int $id
     * 
     * @return array $data
     * 
     * $data 
     *    "ok" -> boolean -> Indica se o procedimento funcionou
     *    "errors" -> array -> Indica possiveis erros no procedimento
     *    "data" -> boolean -> Retorna se o delete funcionou
     * 
    */
    function delete($id) {
        
        $in_transaction = false;
        $data = [
            'ok'=>false,
            'errors'=>[],
            'data'=>false
        ];

        // validar inputs
        $resp = (new Inputs)->verify_numbers([
            ['Id', $id, '+*']
        ]);
            
        $data['errors'] = array_merge($data['errors'], $resp);

        if($this->conn->inTransaction()):
            $in_transaction = true;
        endif;

        if(empty($data['errors'])):
            try {

                if(!$in_transaction):
                    $this->conn->beginTransaction();
                endif;
                
                if(empty($data['errors'])):
                    $sql = '
                        DELETE FROM 
                            pro_cat
                        WHERE
                            pc_fk_categoria = :id_categoria
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_categoria', $id, PDO::PARAM_INT);                        
                    $stmt->execute();
                    
                    $sql = '
                        DELETE FROM
                            categorias
                        WHERE
                            cat_pk = :id_categoria
                    ';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_categoria', $id, PDO::PARAM_INT);                        
                    $stmt->execute();

                endif;    
                
            } catch(Exception $e){
                $data['errors']['db'] = 'Erro no banco de dados: '.$e;
            }
        endif;

        if(empty($data['errors'])):
            $data['ok'] = true;
            $data['data'] = true;
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->commit();
                endif;
            endif;
        else:
            if(!$in_transaction):
                if($this->conn->inTransaction()):
                    $this->conn->rollback(); 
                endif;
            endif;
        endif;

        return $data;
    }
}
