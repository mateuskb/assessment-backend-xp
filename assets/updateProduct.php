<?php include_once("../ws/classes/Categorias.php") ?>
<?php include_once("../ws/classes/Produtos.php") ?>

<!doctype html>
<html ⚡>

<head>
    <title>Webjump | Backend Test | Add Product</title>
    <meta charset="utf-8">

    <link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
    <meta name="viewport" content="width=device-width,minimum-scale=1">
    <style amp-boilerplate>
        body {
            -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            animation: -amp-start 8s steps(1, end) 0s 1 normal both
        }

        @-webkit-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-moz-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-ms-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-o-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }
    </style><noscript>
        <style amp-boilerplate>
            body {
                -webkit-animation: none;
                -moz-animation: none;
                -ms-animation: none;
                animation: none
            }
        </style>
    </noscript>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
</head>
<!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
    <div class="close-menu">
        <a on="tap:sidebar.toggle">
            <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
        </a>
    </div>
    <a href="dashboard.php"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
    <div>
        <ul>
            <li><a href="categories.php" class="link-menu">Categorias</a></li>
            <li><a href="products.php" class="link-menu">Produtos</a></li>
        </ul>
    </div>
</amp-sidebar>
<header>
    <div class="go-menu">
        <a on="tap:sidebar.toggle">☰</a>
        <a href="dashboard.php" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
    </div>
    <div class="right-box">
        <span class="go-title">Administration Panel</span>
    </div>
</header>
<!-- Header -->
<!-- Main Content -->
<main class="content">
    <?php
    $success = isset($_GET["success"]) ? $_GET["success"] : "";
    if ($success) :
        if ($success == "1") :
            echo '<script type="text/javascript">alert("Product successfully updated");</script>';
        else :
            echo '<script type="text/javascript">alert("Error updating product");</script>';
        endif;
    endif;
    $success_image = isset($_GET["successImage"]) ? $_GET["successImage"] : "";
    if ($success_image) :
        if ($success_image == "1") :
            echo '<script type="text/javascript">alert("Image successfully deleted");</script>';
        else :
            echo '<script type="text/javascript">alert("Error deleting Image");</script>';
        endif;
    endif;
    ?>
    <h1 class="title new-item">Update Product</h1>

    <?php 
        $resp = (new Produtos)->ler_produto($_GET["id"]);
        if(isset($resp)):
            if($resp["ok"]):
                $produto = $resp["data"];
                $categorias = [];
                foreach($produto["categorias"] as $categoria):
                    $categorias[] = $categoria["cat_pk"];
                endforeach;
            endif;
        endif;
    ?>
    <form method="POST" enctype="multipart/form-data" action="<?php echo REQUESTS["updateProduct"]."?id=".$_GET["id"] ?>">
        <div class="input-field">
            <label for="sku" class="label">Product SKU</label>
            <input type="text" id="sku" class="input-text" name="sku" value='<?php echo $produto["pro_c_sku"]?>'/>
        </div>
        <div class="input-field">
            <label for="nome" class="label">Product Name</label>
            <input type="text" id="nome" class="input-text" name="nome" value='<?php echo $produto["pro_c_nome"]?>'/>
        </div>
        <div class="input-field">
            <label for="price" class="label">Price</label>
            <input type="text" id="price" class="input-text" name="price" value='<?php echo $produto["pro_d_price"]?>'/>
        </div>
        <div class="input-field">
            <label for="quantity" class="label">Quantity</label>
            <input type="text" id="quantity" class="input-text" name="quantidade" value='<?php echo $produto["pro_i_quantidade"]?>'/>
        </div>
        <div class="input-field">
            <label for="category" class="label">Categories</label>
            <select multiple='multiple' id="category" class="input-text" name="categorias[]">
                <?php
                $resp = (new Categorias)->ler();
                if ($resp["ok"]) {
                    foreach ($resp["data"] as $categoria) :
                        var_dump(in_array($categoria["cat_pk"], $categorias));
                        if(in_array($categoria["cat_pk"], $categorias)):
                            echo "<option value=" . $categoria["cat_pk"] . " selected>" . $categoria["cat_c_nome"] . "</option>"; 
                        else:
                            echo "<option value=" . $categoria["cat_pk"] . ">" . $categoria["cat_c_nome"] . "</option>"; 
                        endif;
                    endforeach;
                }
                ?>
            </select>
        </div>
        <div class="input-field">
            <label for="description" class="label">Description</label>
            <textarea id="description" class="input-text" name="descricao"><?php echo $produto["pro_t_descricao"]?></textarea>
        </div>
        <div class="input-field">
            <label for="image" class="label">New Image</label>
            <input id="image" class="input-text" name="imagem" type="file" ></input>
            <?php
                if(!empty($produto["pro_c_imagem"])):
                    echo "
                        <img style='width: 100px;height: auto; margin: 10px 30px;' src='".UPLOAD_IMAGES.$produto['pro_c_imagem']."' alt='imagem_".$produto['pro_c_nome']."'/>
                        <a href='".REQUESTS['delImage'].'?id='.$produto['pro_pk']."'>Deletar imagem</a>
                    ";
                endif;
            ?>
        </div>
        
        <div class="actions-form">
            <a href="products.php" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Update Product" />
        </div>

    </form>
</main>
<!-- Main Content -->

<!-- Footer -->
<footer>
    <div class="footer-image">
        <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
    </div>
    <div class="email-content">
        <span>go@jumpers.com.br</span>
    </div>
</footer>
<!-- Footer -->
</body>

</html>