-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 10-Mar-2021 às 21:33
-- Versão do servidor: 10.4.14-MariaDB
-- versão do PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `assessment`
--
CREATE DATABASE IF NOT EXISTS `assessment` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `assessment`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `cat_pk` int(11) NOT NULL,
  `cat_c_nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `cat_c_code` varchar(120) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `pro_pk` int(11) NOT NULL,
  `pro_c_sku` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `pro_c_nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `pro_i_quantidade` int(11) NOT NULL,
  `pro_d_price` decimal(20,2) NOT NULL,
  `pro_dt_criado_em` datetime NOT NULL,
  `pro_t_descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `pro_c_imagem` varchar(240) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pro_cat`
--

CREATE TABLE `pro_cat` (
  `pc_pk` int(11) NOT NULL,
  `pc_fk_produto` int(11) NOT NULL,
  `pc_fk_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`cat_pk`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`pro_pk`);

--
-- Índices para tabela `pro_cat`
--
ALTER TABLE `pro_cat`
  ADD PRIMARY KEY (`pc_pk`),
  ADD KEY `fk_pc_produto` (`pc_fk_produto`),
  ADD KEY `fk_pc_categoria` (`pc_fk_categoria`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `categorias`
--
ALTER TABLE `categorias`
  MODIFY `cat_pk` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `pro_pk` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `pro_cat`
--
ALTER TABLE `pro_cat`
  MODIFY `pc_pk` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `pro_cat`
--
ALTER TABLE `pro_cat`
  ADD CONSTRAINT `fk_pc_categoria` FOREIGN KEY (`pc_fk_categoria`) REFERENCES `categorias` (`cat_pk`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pc_produto` FOREIGN KEY (`pc_fk_produto`) REFERENCES `produtos` (`pro_pk`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
