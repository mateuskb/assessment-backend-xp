
# nome: MATEUS HENRIQUE RODRIGUES RIBEIRO
# email: mateuskb@edu.uniube.br

# Como rodar o projeto
- Tenha instalado as tecnologias especificadas acima e na versão correta para não causar erros.
- Execute o SQL  "assessment-backend-xp/estrutura.sql" no PHPMYADMIN para criar o banco de dados.
- Tenha certeza que o diretório principal **'assessment-backend-xp'** esteja em "C:\xampp\htdocs\" ou similar caso o disco de isntalação seja diferente.
- Ative o APACHE e o MYSQL no XAMPP Control Panel.
- Acesse o dashboard através de "http://localhost/assessment-backend-xp/assets/dashboard.php" pelo navegador.
- Caso queira rodar o teste automatizado, acesse-o através de "http://localhost/assessment-backend-xp/ws/requests/automatizado.php" pelo navegador, ele pode demorar alguns segundos para rodar.

# Informações importantes
- Para a facilitação dos teste que serão realizados, alterei os arquivos da pasta assets para que eles se tornem parte da aplicação, podendo, então, testar todas as funções CRUD através do UI
- Os arquivos foram alterados para PHP, no entanto, essa alteração não é mais padrão no mercado, mas, uma vez que o foco do desafio era o CRUD, prezei pela praticidade e não utilizei nenhuma ferramenta ou biblioteca javascript para fazer requisições ao PHP através de arquivos JSON.
- A pasta 'ws' teve ser nome derivado de 'Web service' e lá encontra-se todos os arquivos necessários para se fazer o crud.
- O teste automatizado irá popular o banco de dados com os dados do arquivo "import.csv"

# Tecnologias e versões
- **XAMPP Control Panel** - v3.2.4+
- **PHP** - v7.2.34+
- **pdo_mysql** - v5.0.12-dev+
- **Apache** - v2.4.46
- **Phpmyadmin** - v5.0.3
